CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The Hierarchial Taxonomy Autocomplete module provides a way to distinctly select
terms in a vocabulary when tagging content.
 * For a full description of the module, visit the project page:
   TBD
 * To submit bug reports and feature suggestions, or to track changes:
   TBD

REQUIREMENTS
------------
This module requires the following modules:
 * Field (field)
 * Field UI (field_ui)
 * System (system)
 * Taxonomy (taxonomy)

Recommended modules
-------------------
 * There are no recommended modules at this time.

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
The module has no menu or modifiable settings.  There is no
configuration.  When enabled, the module will provide a new field type
called "Term reference for custom taxonomy autocomplete" and a new widget
called "Hierarchial taxonomy autocomplete widget (tagging)".
Manage fields on a content type to see these options.

TROUBLESHOOTING
---------------
 * There are no troubleshooting steps at this time.

FAQ
---
 * There are no FAQs at this time.

MAINTAINERS
-----------
Current maintainers:
 * Jennifer Hoffman (spajennios) - https://www.drupal.org/user/2357972
