(function ($) {
  /**
   * Attaches the autocomplete behavior to all required fields.
   */
  Drupal.behaviors.hierarchial_taxonomy_autocomplete = {
    attach: function (context, settings) {
      var acdb = [];
      $('input.hierarchial-taxonomy-autocomplete', context).once('hierarchial-taxonomy-autocomplete', function () {
        var suffix = '-hierarchial-taxonomy-autocomplete';
        var uri = this.value;
        if (!acdb[uri]) {
          acdb[uri] = new Drupal.hierarchial_taxonomy_autocomplete_ACDB(uri);
        }
        var $input = $('#' + this.id.substr(0, this.id.length - suffix.length))
          .attr('autocomplete', 'OFF')
          .attr('aria-autocomplete', 'list');
        $($input[0].form).submit(Drupal.autocompleteSubmit);

        if ($input[0].value != '') {
          $('#' + this.id.substr(0, this.id.length - suffix.length)).attr('readonly', true);
        }

        $input.parent()
          .attr('role', 'application')
          .append($('<span class="element-invisible" aria-live="assertive"></span>')
            .attr('id', $input.attr('id') + '-autocomplete-aria-live')
          );
        // Add the hierarchy span if it does not already exist.
        if ($('#' + $input.attr('id') + '-hierarchy').length == 0) {
          $input.parent().append($('<span></span>')
            .attr('id', $input.attr('id') + '-hierarchy')
          );
        }

        // If the term is populated, show the clear button.
        if ($input[0].value != '') {
          $input.siblings('button').removeClass('clear-hierarchial-taxonomy-autocomplete-hidden');
        }
        // Add the clear button if it does not already exist.
        // This is a quick fix to "delete" a tag.
        $input.siblings('button')
          .click(function() {
            $.each($(this).siblings('input[type="text"]'), function(index, value) {
              // Hide the input value.
              this.value = '';
              $(this).removeAttr('readonly');
            });

            // Hide the hierarchy.
            $(this).siblings('#' + $input.attr('id') + '-hierarchy').html('');
            $(this).addClass('clear-hierarchial-taxonomy-autocomplete-hidden');
            return false;
          });
        new Drupal.hierarchial_taxonomy_autocomplete_jsAC($input, acdb[uri]);
      });
    }
  };

  /**
   * Custom autoComplete object.
   */
  Drupal.hierarchial_taxonomy_autocomplete_jsAC = function ($input, db) {
    var ac = this;
    this.input = $input[0];
    this.ariaLive = $('#' + this.input.id + '-autocomplete-aria-live');
    this.db = db;

    // Stock event handlers from autocomplete.js
    $input
      .keydown(function (event) { return ac.onkeydown(this, event); })
      .keyup(function (event) { ac.onkeyup(this, event); })
      .blur(function () { ac.hidePopup(); ac.db.cancel(); });

  };

  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.onkeydown = Drupal.jsAC.prototype.onkeydown;
  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.onkeyup = Drupal.jsAC.prototype.onkeyup;
  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.select = function (node) {
    this.input.value = $(node).data('autocompleteValue');
    // Display the term's hierarchy.
    var hierarchy_key = this.input.id + '-hierarchy';
    $('#' + hierarchy_key).html($(node).data('autocompleteHierarchy'));

    // Disable the input field when a term is selected.
    $('#' + this.input.id).attr('readonly', true);

    // Enable the clear button when a term is selected.
    // This is a quick fix to "delete" a tag.
    $('#' + this.input.id).siblings('button').removeClass('clear-hierarchial-taxonomy-autocomplete-hidden');
  };
  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.selectDown = Drupal.jsAC.prototype.selectDown;
  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.selectUp = Drupal.jsAC.prototype.selectUp;
  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.highlight = Drupal.jsAC.prototype.highlight;
  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.unhighlight = Drupal.jsAC.prototype.unhighlight;
  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.hidePopup = function (keycode) {
    // Select item if the right key or mousebutton was pressed.
    if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
      this.input.value = $(this.selected).data('autocompleteValue');
    }
    else if ($('#' + this.input.id).attr('readonly') === false) {
      this.input.value = '';
    }
    // Hide popup.
    var popup = this.popup;
    if (popup) {
      this.popup = null;
      $(popup).fadeOut('fast', function () { $(popup).remove(); });
    }
    this.selected = false;
    $(this.ariaLive).empty();
  };

  /**
   * Positions the suggestions popup and starts a search.
   */
  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.populatePopup = function () {
    var $input = $(this.input);
    var position = $input.position();
    // Show popup.
    if (this.popup) {
      $(this.popup).remove();
    }
    this.selected = false;
    this.popup = $('<div id="hierarchial-taxonomy-autocomplete"></div>')[0];
    this.popup.owner = this;
    $(this.popup).css({
      top: parseInt(position.top + this.input.offsetHeight, 10) + 'px',
      left: parseInt(position.left, 10) + 'px',
      width: $input.innerWidth() + 'px',
      display: 'none'
    });
    $input.before(this.popup);

    // Do search.
    this.db.owner = this;
    this.db.search(this.input.value);
  };

  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.found = function (matches) {
    // If no value in the textfield, do not show the popup.
    if (!this.input.value.length) {
      return false;
    }

    // Prepare matches.
    var ul = $('<ul></ul>');
    var ac = this;
    for (key in matches) {
      if (typeof matches[key] === 'object' && matches[key].value != 'undefined' && matches[key].label != 'undefined') {
        $('<li></li>')
          .html($('<div></div>').html(matches[key].label))
          .mousedown(function () { ac.select(this); })
          .mouseover(function () { ac.highlight(this); })
          .mouseout(function () { ac.unhighlight(this); })
          .data('autocompleteValue', matches[key].value + ' [term-id: ' + matches[key].tid + ']')
          .data('autocompleteHierarchy', matches[key].label)
          .appendTo(ul);
      }
    }

    // Show popup with matches, if any.
    if (this.popup) {
      if (ul.children().length) {
        $(this.popup).empty().append(ul).show();
        $(this.ariaLive).html(Drupal.t('Autocomplete popup'));
      }
      else {
        $(this.popup).css({ visibility: 'hidden' });
        this.hidePopup();
      }
    }
  };

  Drupal.hierarchial_taxonomy_autocomplete_jsAC.prototype.setStatus = Drupal.jsAC.prototype.setStatus;

  Drupal.hierarchial_taxonomy_autocomplete_ACDB = Drupal.ACDB;

  Drupal.hierarchial_taxonomy_autocomplete_ACDB.prototype.search = function (searchString) {
    var db = this;
    this.searchString = searchString;

    // See if this string needs to be searched for anyway.
    searchString = searchString.replace(/^\s+|\s+$/, '');
    if (searchString.length <= 0 ||
      searchString.charAt(searchString.length - 1) == ',') {
      return;
    }

    // See if this key has been searched for before.
    if (this.cache[searchString]) {
      return db.owner.found(this.cache[searchString]);
    }

    // Initiate delayed search.
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(function () {
      db.owner.setStatus('begin');
      var uri = (db.uri.indexOf('?') > -1) ? db.uri.substring(0, db.uri.indexOf('?')) : db.uri;
      // Ajax GET request for autocompletion.
      $.ajax({
        type: 'GET',
        url: uri + '/' + Drupal.encodePath(searchString),
        dataType: 'json',
        success: function (matches) {
          if (typeof matches.status == 'undefined' || matches.status != 0) {
            db.cache[searchString] = matches;
            // Verify if these are still the matches the user wants to see.
            if (db.searchString == searchString) {
              db.owner.found(matches);
            }
            db.owner.setStatus('found');
          }
        },
        error: function (xmlhttp) {
          if (console !== undefined && console.log !== undefined) {
            console.log(Drupal.ajaxError(xmlhttp, db.uri));
          }
        }
      });
    }, this.delay);
  };

  Drupal.hierarchial_taxonomy_autocomplete_ACDB.prototype.cancel = Drupal.ACDB.prototype.cancel;
})(jQuery);
