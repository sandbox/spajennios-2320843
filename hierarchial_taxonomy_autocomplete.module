<?php
/**
 * @file
 * hierarchial_taxonomy_autocomplete.module
 */

define('HIERARCHIAL_TAXONOMY_AUTOCOMPLETE_MAX_RESULTS', 20);

/**
 * Implements hook_field_info().
 *
 * Define Field API field types.
 */
function hierarchial_taxonomy_autocomplete_field_info() {
  return array(
    'taxonomy_term_reference_for_hierarchial_taxonomy_autocomplete' => array(
      'label' => t('Term reference for hierarchial taxonomy autocomplete'),
      'description' => t('This field stores a reference to a taxonomy term.  Similar to term reference.  Use this field if you want to use the hierarchial taxonomy autocomplete widget (tagging) widget.'),
      'default_widget' => 'hierarchial_taxonomy_autocomplete_taxonomy_hierarchial_taxonomy_autocomplete',
      'default_formatter' => 'taxonomy_term_reference_for_hierarchial_taxonomy_autocomplete_formatter',
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => '',
            'parent' => '0',
          ),
        ),
        // @TODO: Implement this.
        'allow_term_autocreation' => FALSE,
        // @TODO: Implement this.
        'max_values' => HIERARCHIAL_TAXONOMY_AUTOCOMPLETE_MAX_RESULTS,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 *
 * Expose Field API widget types.
 */
function hierarchial_taxonomy_autocomplete_field_widget_info() {
  return array(
    'hierarchial_taxonomy_autocomplete_taxonomy_hierarchial_taxonomy_autocomplete' => array(
      'label' => t('hierarchial taxonomy autocomplete widget (tagging).'),
      'field types' => array('taxonomy_term_reference_for_hierarchial_taxonomy_autocomplete'),
      'settings' => array(
        'size' => 60,
        'autocomplete_path' => 'hierarchial_taxonomy_autocomplete/taxonomy/',
      ),
      'behaviors' => array(
        'default value' => FIELD_BEHAVIOR_NONE,
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_info_alter().
 *
 * Perform alterations on Field API widget types.
 */
function hierarchial_taxonomy_autocomplete_field_widget_info_alter(&$info) {
  $widgets = array(
    'hierarchial_taxonomy_autocomplete_taxonomy_hierarchial_taxonomy_autocomplete' => array('taxonomy_term_reference_for_hierarchial_taxonomy_autocomplete'),
  );
  foreach ($widgets as $widget => $field_types) {
    $info[$widget]['field types'] = array_merge($info[$widget]['field types'], $field_types);
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 *
 * Alter widget forms for a specific widget provided by another module.
 */
function hierarchial_taxonomy_autocomplete_field_widget_hierarchial_taxonomy_autocomplete_taxonomy_hierarchial_taxonomy_autocomplete_form_alter(&$element, &$form_state, $context) {
  // Disable weights.
  unset($element['_weight']);

  // Disable dragging.
  $element['#nodrag'] = TRUE;
}

/**
 * Implements hook_field_widget_form().
 *
 * Return the form for a single field widget.
 */
function hierarchial_taxonomy_autocomplete_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $machine_name = $field['settings']['allowed_values'][0]['vocabulary'];
  if (!$machine_name) {
    return NULL;
  }
  $vocabulary = taxonomy_vocabulary_machine_name_load($machine_name);
  if (!is_object($vocabulary)) {
    return NULL;
  }

  $tags = array();
  foreach ($items as $index => $item) {
    $tags[$index] = isset($item['taxonomy_term']) ? $item['taxonomy_term'] : taxonomy_term_load($item['tid']);
    $display_name = '';
    if (is_object($tags[$index])) {
      if ($tags[$index]->name && $tags[$index]->tid) {
        $display_name = $tags[$index]->name . ' [term-id: ' . $tags[$index]->tid . ']';
      }
      $tags[$index]->display_name = $display_name;
    }
  }

  $default_display_name = '';
  if (array_key_exists($delta, $tags) && is_object($tags[$delta])) {
    // This function is called for each textfield in the group.
    // Use delta to get the term for each field.
    $default_display_name = $tags[$delta]->display_name;
  }

  $module_path = drupal_get_path('module', 'hierarchial_taxonomy_autocomplete');
  // Adding specific js and styles for autocomplete.
  drupal_add_js(
    "misc/autocomplete.js",
    array('group' => JS_LIBRARY, 'weight' => 0)
  );
  drupal_add_js(
    $module_path . '/hierarchial_taxonomy_autocomplete.js',
    array('group' => JS_DEFAULT)
  );
  drupal_add_css($module_path . '/hierarchial_taxonomy_autocomplete.css');

  // We need to set default_value, not value to make sure values remain when
  // the "add more items" button is clicked.
  $element += array(
    '#type' => 'hierarchial_taxonomy_autocomplete_textfield',
    '#default_value' => $default_display_name,
    '#autocomplete_path' => $instance['widget']['settings']['autocomplete_path'] . $vocabulary->vid,
    '#size' => $instance['widget']['settings']['size'],
    '#element_validate' => array('hierarchial_taxonomy_autocomplete_taxonomy_autocomplete_validate'),
    '#attributes' => array('class' => array('form-autocomplete')),
    '#attached' => array(
      'css' => array(
        'data' => $module_path . '/hierarchial_taxonomy_autocomplete.css',
        'type' => 'file',
      ),
      'js' => array(
        $module_path . '/hierarchial_taxonomy_autocomplete.js' => array(
          'type' => 'file',
          'group' => JS_DEFAULT,
        ),
        'misc/autocomplete.js' => array(
          'type' => 'file',
          'group' => JS_LIBRARY,
          'weight' => 0,
        ),
      ),
    ),
    '#suffix' => '<button class="clear-hierarchial-taxonomy-autocomplete clear-hierarchial-taxonomy-autocomplete-hidden">Clear</button>',
  );

  return $element;
}

/**
 * Implements hook_field_validate().
 *
 * Validate this module's field data.
 */
function hierarchial_taxonomy_autocomplete_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  // Bypass the validation when viewing the widget on the edit field page.
  if (!$entity_type) {
    return;
  }

  $is_valid = FALSE;
  $tids = array();
  foreach ($items as $delta => $item) {
    if (is_array($item)) {
      if (!empty($item['tid'])) {
        $tids[] = $item['tid'];
      }
    }
  }

  if ($tids) {
    // If there are TIDs, validate them.
    $term = taxonomy_term_load($tids[0]);
    if (is_object($term)) {
      $is_valid = TRUE;
    }
  }
  elseif (!$instance['required']) {
    // If there are no TIDs and the field is not required, that is ok.
    $is_valid = TRUE;
  }

  if (!$is_valid) {
    $errors[$field['field_name']][$langcode][$delta][] = array(
      'error' => 'taxonomy_term_reference_for_hierarchial_taxonomy_autocomplete_illegal_value',
      'message' => t('%name: illegal value.', array('%name' => $instance['label'])),
    );
  }
}

/**
 * Implements hook_field_formatter_info().
 *
 * Expose Field API formatter types.
 */
function hierarchial_taxonomy_autocomplete_field_formatter_info() {
  return array(
    'taxonomy_term_reference_for_hierarchial_taxonomy_autocomplete_formatter' => array(
      'label' => t('hierarchial taxonomy autocomplete field'),
      'field types' => array('taxonomy_term_reference_for_hierarchial_taxonomy_autocomplete'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 *
 * Build a renderable array for a field value.
 */
function hierarchial_taxonomy_autocomplete_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'taxonomy_term_reference_for_hierarchial_taxonomy_autocomplete_formatter':
      drupal_add_css(drupal_get_path('module', 'hierarchial_taxonomy_autocomplete') . '/hierarchial_taxonomy_autocomplete.css');
      foreach ($items as $delta => $item) {
        $hierarchy = _hierarchial_taxonomy_autocomplete_get_hierarchy($item['tid']);
        $vars = array(
          'title' => '',
          'type' => 'ul',
          'attributes' => array('class' => 'display-hierarchy'),
          'items' => $hierarchy,
        );

        $element[$delta] = array(
          '#markup' => theme_item_list($vars),
        );
      }
      break;
  }
  return $element;
}

/**
 * Implements hook_field_is_empty().
 *
 * Define what constitutes an empty item for a field type.
 */
function hierarchial_taxonomy_autocomplete_field_is_empty($item, $field) {
  if (!is_array($item) || (array_key_exists('tid', $item) && empty($item['tid']) && (string) $item['tid'] !== '0')) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_settings_form().
 *
 * Add settings to a field settings form.
 */
function hierarchial_taxonomy_autocomplete_field_settings_form($field, $instance, $has_data) {
  // Get proper values for 'allowed_values_function', which is a core setting.
  $vocabularies = taxonomy_get_vocabularies();
  $options = array();
  foreach ($vocabularies as $vocabulary) {
    $options[$vocabulary->machine_name] = $vocabulary->name;
  }
  $form['allowed_values'] = array(
    '#tree' => TRUE,
  );

  foreach ($field['settings']['allowed_values'] as $delta => $tree) {
    $form['allowed_values'][$delta]['vocabulary'] = array(
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#default_value' => $tree['vocabulary'],
      '#options' => $options,
      '#required' => TRUE,
      '#description' => t('The vocabulary which supplies the options for this field.'),
      '#disabled' => $has_data,
    );

    $form['allowed_values'][$delta]['parent'] = array(
      '#type' => 'value',
      '#value' => $tree['parent'],
    );
  }

  $form['allow_term_autocreation'] = array(
    '#markup' => t('<strong>Free Tagging</strong><br/>Free tagging is disabled.  New terms are not automatically created for unknown values.  Terms must be chosen from the provided drop down list.'),
  );

  $form['max_results'] = array(
    '#markup' => t('<strong>Maximum Results</strong><br/>This is currently set in code to ' . HIERARCHIAL_TAXONOMY_AUTOCOMPLETE_MAX_RESULTS . '.  This will be configurable in a future release.'),
  );

  return $form;
}

/**
 * Implements hook_element_info().
 *
 * Allows modules to declare their own Form API element types and specify their
 *   default values.  We need to create a custom form element so we don't
 *   clobber the existing autocomplete functionality.
 */
function hierarchial_taxonomy_autocomplete_element_info() {
  $types['hierarchial_taxonomy_autocomplete_textfield'] = array(
    '#input' => TRUE,
    '#size' => 60,
    '#maxlength' => 128,
    '#autocomplete_path' => FALSE,
    '#process' => array('ajax_process_form'),
    '#theme' => 'hierarchial_taxonomy_autocomplete_textfield',
  );
  return $types;
}

/**
 * Implements hook_theme().
 *
 * Register the custom element for theming.
 */
function hierarchial_taxonomy_autocomplete_theme() {
  return array(
    'hierarchial_taxonomy_autocomplete_textfield' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function hierarchial_taxonomy_autocomplete_menu() {
  $items = array();

  // The callback to get the taxonomy data.
  $items['hierarchial_taxonomy_autocomplete/taxonomy/%'] = array(
    'title' => 'hierarchial taxonomy autocomplete',
    'page callback' => 'hierarchial_taxonomy_autocomplete_term_data_search',
    'page arguments' => array(2),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Form element validate handler.
 *
 * For hierarchial_taxonomy_autocomplete_taxonomy_hierarchial_taxonomy_autocomplete element.
 */
function hierarchial_taxonomy_autocomplete_taxonomy_autocomplete_validate($element, &$form_state) {
  // NOTE: This widget only handles one taxonomy term at a time.
  $tag = $element['#value'];
  if ($tag) {
    if (!is_array($element['#value'])) {
      $tid = _hierarchial_taxonomy_autocomplete_parse_tid($element['#value']);
      if (is_numeric($tid)) {
        $term = taxonomy_term_load($tid);
        $value = (array) $term;

        form_set_value($element, $value, $form_state);
      }
      else {
        form_set_error($element['#field_name'], t('You must pick a term from the autocomplete list.'));
      }
    }
  }
}

/**
 * Returns HTML for a hierarchial_taxonomy_autocomplete_textfield form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #title, #value, #description, #size, #maxlength,
 *     #required, #attributes, #autocomplete_path.
 *
 * @ingroup themeable
 */
function theme_hierarchial_taxonomy_autocomplete_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes(
    $element,
    array('id', 'name', 'value', 'size', 'maxlength')
  );
  _form_set_class($element, array('form-text'));

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-hierarchial-taxonomy-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'hierarchial-taxonomy-autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
  return $output . $extra;
}

/**
 * Menu callback.
 *
 * Returns taxonomy search results based on a search string and vid.
 *
 * @param string $search
 *   A string containing the search term.
 *
 * @return object
 *   The results of the search with the appropriate hierarchy themed, the
 *    term name, and tid.
 */
function hierarchial_taxonomy_autocomplete_term_data_search($vid, $search) {
  $results = array();
  if (strlen($search) > 2 && isset($vid)) {
    // Get the cached list of all terms.
    $term_data_all = hierarchial_taxonomy_autocomplete_get_taxonomy_term_data($vid);
    foreach ($term_data_all as $tid => $term_obj) {
      if (strripos($term_obj['name'], $search) !== FALSE) {
        similar_text(strtolower($term_obj['name']), $search, $percent);
        $hierarchy = _hierarchial_taxonomy_autocomplete_get_hierarchy($term_obj['tid']);
        $vars = array(
          'title' => '',
          'type' => 'ul',
          'attributes' => array('class' => 'display-hierarchy'),
          'items' => $hierarchy,
        );

        $results[$term_data_all[$term_obj['tid']]['name'] . $term_data_all[$term_obj['tid']]['tid']] = array(
          'label' => theme_item_list($vars),
          'value' => $term_data_all[$term_obj['tid']]['name'],
          'tid' => $term_obj['tid'],
          'similarity' => $percent,
          'hierarchy' => theme_item_list($vars),
        );
      }
    }

    // Don't bother running this loop if the result list would be too long to
    // be useful.
    if (count($results) < HIERARCHIAL_TAXONOMY_AUTOCOMPLETE_MAX_RESULTS) {
      foreach ($results as $name => $result) {
        // If we get a result, return that term and all
        // the children of that term.
        if ($term_data_all[$result['tid']]->child_tids) {
          foreach ($term_data_all[$result['tid']]->child_tids as $child) {
            similar_text(strtolower($term_data_all[$child]->name), $search, $percent);
            $hierarchy = _hierarchial_taxonomy_autocomplete_get_hierarchy($child);
            $vars = array(
              'title' => '',
              'type' => 'ul',
              'attributes' => array('class' => 'display-hierarchy'),
              'items' => $hierarchy,
            );

            $results[$term_data_all[$child]->name . $term_data_all[$child]->tid] = array(
              'label' => theme_item_list($vars),
              'value' => $term_data_all[$child]['name'],
              'tid' => $child,
              'similarity' => $percent,
              'hierarchy' => theme_item_list($vars),
            );
          }
        }
      }
    }
    // Obtain a list of columns.
    foreach ($results as $key => $row) {
      $similarity[$key]  = $row['similarity'];
    }

    // Sort the data with similarity descending.
    array_multisort($similarity, SORT_DESC, SORT_NUMERIC, $results);
  }

  // Return the first HIERARCHIAL_TAXONOMY_AUTOCOMPLETE_MAX_RESULTS.
  drupal_json_output(array_slice($results, 0, HIERARCHIAL_TAXONOMY_AUTOCOMPLETE_MAX_RESULTS));
}

/**
 * Get all of the terms in a given vocabulary.
 *
 * Cache the result if not previously cached.
 *
 * @param int $vid
 *   The vid of the vocabulary.
 *
 * @return object
 *   All term data for a vocabulary.
 */
function hierarchial_taxonomy_autocomplete_get_taxonomy_term_data($vid) {
  // Get a list of all potential terms to search against.
  $term_data_all = cache_get('term_data_all_' . $vid);
  // Build the term data group array and cache the result.
  if (empty($term_data_all->data)) {
    $term_list = entity_load('taxonomy_term', FALSE, array('vid' => $vid));

    $term_data = array();
    $term_data_all = array();

    if (!empty($term_list)) {
      foreach ($term_list as $term) {
        $term_data = array(
          'name' => trim($term->name),
          'tid' => $term->tid,
        );

        // Get the tree for each term which represents all of its children.
        // @TODO: Do we need to write a custom wrapper for performance?
        $children = taxonomy_get_tree($term->tid, $vid);

        // Build a contextual hierarchy for a given term and
        // for each node (child) in the tree, get its parents up to the term.
        foreach ($children as $child) {
          $term_data['child_tids'][] = $child->tid;
        }

        $hier = array();
        $parents = taxonomy_get_parents_all($term->tid);
        foreach ($parents as $key => $p) {
          $hier[] = $p->name;
        }
        $term_data['output'] = array_values($hier);
        $term_data_all[$term->tid] = $term_data;
      }
      cache_set('term_data_all_' . $vid, $term_data_all, 'cache', 120);
    }
  }
  return is_object($term_data_all) ? $term_data_all->data : $term_data_all;
}

/**
 * Implements hook_theme_registry_alter().
 *
 * Alter the theme registry information returned from hook_theme().
 */
function hierarchial_taxonomy_autocomplete_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['field_multiple_value_form'])) {
    $theme_registry['field_multiple_value_form']['type'] = 'module';
    $theme_registry['field_multiple_value_form']['theme path'] = drupal_get_path('module', 'hierarchial_taxonomy_autocomplete');
    $theme_registry['field_multiple_value_form']['function'] = 'hierarchial_taxonomy_autocomplete_theme_field_multiple_value_form';
  }
}

/**
 * Theme function override for multiple-value form widgets.
 *
 * @see theme_field_multiple_value_form()
 */
function hierarchial_taxonomy_autocomplete_theme_field_multiple_value_form($variables) {
  $element = $variables['element'];
  $output = '';

  // We only want to override fields where #notrag is set to true.
  // Fallback to theme_field_multiple_value_form otherwise.
  if (($element['#cardinality'] > 1 || $element['#cardinality'] == FIELD_CARDINALITY_UNLIMITED)
    && (isset($element[0]['#nodrag']) && $element[0]['#nodrag'])) {
    $table_id = drupal_html_id($element['#field_name'] . '_values');
    $required = !empty($element['#required']) ? theme('form_required_marker', $variables) : '';

    $header = array(
      array(
        'data' => '<label>' . t('!title !required', array('!title' => $element['#title'], '!required' => $required)) . "</label>",
        'class' => array('field-label'),
      ),
    );
    $rows = array();

    // Sort items according to weight
    $items = array();
    foreach (element_children($element) as $key) {
      if ($key === 'add_more') {
        $add_more_button = &$element[$key];
      }
      else {
        $items[] = &$element[$key];
      }
    }

    // Add the items as table rows.
    foreach ($items as $key => $item) {
      // We don't want the weight to render.
      unset($item['_weight']);

      // Add hierarchy to item.
      if (array_key_exists('#value', $item) && $item['#value'] != '') {
        $tid = _hierarchial_taxonomy_autocomplete_parse_tid($item['#value']);
        if (is_numeric($tid)) {
          $hierarchy = _hierarchial_taxonomy_autocomplete_get_hierarchy($tid);
          $vars = array(
            'title' => '',
            'type' => 'ul',
            'attributes' => array('class' => 'display-hierarchy'),
            'items' => $hierarchy,
          );

          $item['#suffix'] = '<button class="clear-hierarchial-taxonomy-autocomplete clear-hierarchial-taxonomy-autocomplete-hidden">Clear</button>';
          $item['#suffix'] .= '<span id="' . $item['#id'] . '-hierarchy">' . theme_item_list($vars) . '</span>';
        }
      }

      $cells = array(
        drupal_render($item),
      );
      $rows[] = array(
        'data' => $cells,
      );
    }

    $output = '<div class="form-item">';
    $output .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => $table_id,
        'class' => array('field-multiple-table'),
      ),
    ));
    $output .= $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '';
    $output .= '<div class="clearfix">' . drupal_render($add_more_button) . '</div>';
    $output .= '</div>';
  }
  else {
    $output = theme_field_multiple_value_form($variables);
  }

  return $output;
}

/**
 * Helper function that returns a term's hierarchy.
 *
 * @param int $tid
 *   The term's tid.
 *
 * @return array
 *   An array containing a term and it's parents.
 *   The first item in the array is the term.
 */
function _hierarchial_taxonomy_autocomplete_get_hierarchy($tid) {
  $hierarchy = array();
  $parents = taxonomy_get_parents_all($tid);
  if (is_array($parents)) {
    foreach ($parents as $key => $p) {
      $hierarchy[] = $p->name;
    }
  }

  // Get the term, reverse the order of the hierarchy and add the term to the
  // beginning of the hierarchy.
  $term = array_shift($hierarchy);
  $hierarchy = array_reverse($hierarchy);
  array_unshift($hierarchy, $term);

  return $hierarchy;
}

/**
 * Helper function that parses the tid from the form autocomplete value.
 *
 * @param string $name
 *   Example value: red [term-id: 999999]
 *
 * @return int
 *   Example return: 999999
 */
function _hierarchial_taxonomy_autocomplete_parse_tid($input) {
  $tid = NULL;
  preg_match("/\[term-id: (\d+)\]/", $input, $matches);
  if (array_key_exists(1, $matches) && is_numeric($matches[1])) {
    $tid = $matches[1];
  }
  return $tid;
}
